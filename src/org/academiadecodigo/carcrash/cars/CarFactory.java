package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

import java.util.TooManyListenersException;
import java.util.concurrent.ThreadPoolExecutor;

public class CarFactory {
    public static Car getNewCar() {
        double type = Math.random();
        if (type < 0.5) {
            Car car = new BMW();
            car.setPos(new Position());
            return car;
        } else {
            Car car = new TOYOTA();
            car.setPos(new Position());
            return car;
        }
    }
}



