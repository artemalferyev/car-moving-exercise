package org.academiadecodigo.carcrash.cars;
import org.academiadecodigo.carcrash.Game;
import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;

public class Car {
    /**
     * The position of the car on the grid
     */
    private Position pos;

    private boolean isCrashed = false;

    public Position getPos() {
        return pos;
    }

    public Position setPos(Position pos) {
        this.pos = pos;
        return pos;
    }

    public void setCrashed(boolean crashed) {
        isCrashed = crashed;
    }

    public void move() {
        moveInDirections();
    }
    public boolean isCrashed(){
        return isCrashed;
    }

    public void moveInDirections(){
        int randomDirection = (int)(Math.random() * 4);

        switch(randomDirection){
            case 0:
                moveUp();
                break;
            case 1:
                moveDown();
                break;
            case 2:
                moveLeft();
                break;
            case 3:
                moveRight();
                break;
        }
    }
    public void moveUp() {
        int currentPos = pos.getRow();
        if (currentPos > 0) {
            pos.setRow(currentPos - 1);
        }
    }
    public void moveDown() {
        int currentPos = pos.getRow();
        if (currentPos < Field.height - 1) {
            pos.setRow(currentPos + 1);
        }
    }
    public void moveLeft() {
        int currentPos = pos.getCol();
        if (currentPos > 0) {
            pos.setCol(currentPos - 1);
        }
    }

    public void moveRight() {
        int currentPos = pos.getCol();
        if (currentPos < Field.width - 1) {
            pos.setCol(currentPos + 1);
        }
    }


    }




