package org.academiadecodigo.carcrash.field;

public class Position {
    private int col;
    private int row;
    public Position(){

        this.col = (int) (Math.random() * Field.width);
        this.row = (int) (Math.random() * Field.height);
    }
    public int getCol(){
        return col;
    }
    public int getRow(){
        return row;
    }
    public int setCol(int col){
        this.col = col;
        return col;
    }
    public int setRow(int row){
        this.row = row;
        return row;
    }
}
